`ifndef {:UPPERNAME:}_DRIVER_SV
`define {:UPPERNAME:}_DRIVER_SV

class {:NAME:}_driver extends uvm_driver #({:SEQ_ITEM:});
    `uvm_component_utils({:NAME:}_driver)

    // Attributes
    virtual {:INTERFACE:}_if.driver_mp vif;

    // Methods
    extern function new (string name="{:NAME:}_driver", uvm_component parent=null);
    extern function start_of_simulation_phase (uvm_phase phase);
    extern task run_phase (uvm_phase phase);
    extern function phase_ended (uvm_phase phase);
endclass : {:NAME:}_driver

////////////////////////////////////////////////////////////////////////////////
// Implementation
//------------------------------------------------------------------------------
function {:NAME:}_driver::new (string name="{:NAME:}_driver", uvm_component parent=null);
    super.new(name, parent);
endfunction: new

//------------------------------------------------------------------------------
// Print configuration
//
function {:NAME:}_driver::start_of_simulation_phase (uvm_phase phase);
endfunction

//------------------------------------------------------------------------------
// Get and process items
//
task {:NAME:}_driver::run_phase (uvm_phase phase);
    {:INIT_HARDWARE:}
    forever begin
        seq_item_port.get_next_item(req);
        phase.raise_objection();
        drive_{:NAME:}({:ARGS:});
    `ifdef USING_RESPONSE
        {:CONSTRUCT_RSP_ITEM:}
        rsp.set_id_info(req);
        seq_item_port.item_done(rsp);
    `else
        seq_item_port.item_done();
    `endif
        phase.drop_objection();
    end
endtask: run_phase

//------------------------------------------------------------------------------
// Jump Back
//
function {:NAME:}_driver::phase_ended (uvm_phase phase);
endfunction

`endif
